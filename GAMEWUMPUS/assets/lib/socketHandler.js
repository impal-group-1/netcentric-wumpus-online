
client.on('event', function(msg){
   console.log(msg.from , msg.data);
});
client.on('pesanAdmin', function(msg){
   let ans = prompt(msg);
   client.emit('event', " : "+ans);
});


//Syncron map
var synchronize = {ready : false , map : false , player : false};
//let synchronize = {ready : false , map : false , player : false};
//let allReady = false;
//let mapLoaded = false;
//let playerLoaded = false;
client.on('playerPos',function(pos){
	console.log("getPlayer Pos");
	playerPos = pos;
	goldInMap = 2;
	for(let i = 0 ; i < playerPos[myTeamIdx].length; i++){
		if(playerPos[myTeamIdx][i].player.user.name == state.userName){
			myArrayIdx = i;
			break;
		}
	}
	
	//playerLoaded = true;
	synchronize.player = true;
	startGame();
})
client.on('gameMap', function(mapData){
   gameMap = mapData;
   console.log("get map");
   //pushMessage("Map Loaded");
   //mapLoaded = true;
   synchronize.map = true;
   startGame();
   //reffGame.state.start('mainmenu');
   
});
client.on('allPlayerReady', function(){
	console.log( reffGame.state.getCurrentState().key );
	//allReady = true;
	if( reffGame.state.getCurrentState().key ==  'mainmenu'){
		console.log('Game is loaded already')
		return;
	}
	console.log("all player load map already");
	synchronize.ready = true
	startGame();
});

client.on('requestMapData', function(socketID){
   client.emit('responseMapData', socketID ,  {map:gameMap , playerMap:playerPos});
});

function startGame(){
	if(synchronize.map && synchronize.player && synchronize.ready){
		reffGame.state.start('mainmenu');
		synchronize.map = false;
		synchronize.player = false;
		synchronize.ready = false;
	}
}



client.on('playerLifeUpdate' , function(loc){
	playerPos[loc.tim][loc.idx].dead = loc.dead;
	if(loc.tim == myTeamIdx){
		let name = playerPos[loc.tim][loc.idx].player.user.name;
		
		if(loc.dead == true){
			let penyebab = "";
			switch(loc.cause){
				case 1:
					penyebab = "Killed by Wumpus";
				break;
				case 2:
					penyebab = "Get in the Hole";
				break;
				case 3:
					penyebab = "Attacked by Player Enemy";
				break;
			}
			if(loc.idx==myArrayIdx && loc.cause == 3){
				showToastMessage("You die because "+penyebab , 3000);
				Game.MainMenu.prototype.isDead = true;
				Game.MainMenu.prototype.movementLeft = 0;
				pushMessageServer('Server' ,  'You die because '+penyebab);
			}else{
				showToastMessage("Your team member is die because <br>"+"'"+name+"'<br>"+penyebab , 3000);
				pushMessageServer('Server' ,  'Your team member is die because '+penyebab);
			}
			updateStatusList(playerPos[loc.tim][loc.idx]);
			Game.MainMenu.prototype.players[loc.idx].alpha = 0.4;
			
			
		}else{
			showToastMessage("you're team member is back to game <br>"+"'"+name+"'" , 3000);
			
			Game.MainMenu.prototype.players[loc.idx].alpha = 1;
		}
	}else{
		if(loc.dead == true && loc.cause==3){
			let name = playerPos[loc.tim][loc.idx].player.user.name;
			showToastMessage("Your Team Killing <br>"+"'"+name+"'" , 3000);
			pushMessageServer('Server' ,  'Your Team Killing ("'+name+'")');
		}
	}
});


client.on('goldPosUpdate' , function(update){
	//console.log("movement detected");
	for(let i=0; i < update.map.length; i++){
		gameMap[update.map[i].x][update.map[i].y] = update.map[i].val;
	}
	for(let i=0; i < update.player.length; i++){
		playerPos[update.player[i].tim][update.player[i].idx].gold = update.player[i].val;
	}
	isGameFinish();
});

client.on('movementUpdate' , function(dataMovement){
	//console.log("movement detected");
	Game.MainMenu.prototype.reRender(dataMovement.x,dataMovement.y,dataMovement.xLast,dataMovement.yLast, dataMovement.teamIdx,dataMovement.arrayIdx , false);
});

client.on('turn', function(username,timIdx){
	//console.log("Current Turn ",username, state.userName, username == state.userName);
	if(timIdx == myTeamIdx){
		//pushMessage("Turn kita "+username);
		if(username == account.userName){
			pushMessageServer('Server' ,  'Your turn');
		}else{
			pushMessageServer('Server' ,  'Your teamates turn : '+username);
		}
		
	}else{
		pushMessageServer('Server' ,  'your enemy turn : '+username);
	}
	
	if(username == state.userName){
		Game.MainMenu.prototype.turn();
	}
   
});

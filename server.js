var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(express.static('GAMEWUMPUS'))
app.use(express.static('public'))
app.use(bodyParser.json());
app.engine('html', require('ejs').renderFile);



app.get('/', function(req, res){
  res.render('index.html');
});
app.get('/lobby', function(req, res){
	res.render('lobby.html');
});
app.get('/login', function(req, res){
	res.render('login.html');
});
app.get('/prepareroom', function(req, res){
	res.render('gameRoomPrepare.html' );
});
app.get('/play', function(req, res){
	res.render('gameRoomPlay.html');
});



app.get('/room/:rid', function(req, res){
	if(gameRoom[req.params.rid]!=null){
		if(gameRoom[req.params.rid].status != 0){
			res.send({status:1 , gameRoom: (gameRoom[gameRoom[req.params.rid].parent] != null? gameRoom[gameRoom[req.params.rid].parent].name : "") });
		}else{
			res.send({status:0});
		}
	}else{
		res.send({status:-1});
	}
	
	//res.render('gameRoomPlay.html');
});

let gameRoom = [];
let readyRoom =[];

let MatchMaking =  function(){
	console.log("creating match making");
	let match = [];
	for(let i=0; i < readyRoom.length; i++){
		insert(match,readyRoom[i],0);
	}
	let splicer = []
	for(let i=0; i < match.length; i++){
		let a = teamValue(match[i].teamA);
		let b = teamValue(match[i].teamB);
		if(a!=b){
			splicer.push(i);
		}
	}
	//console.log(match);
	for(let i= splicer.length-1; i>=0; i--){
		match.splice(splicer[i],1);
	}
	console.log(match);
	for(let i=0; i < match.length; i++){
		let roomA = collabTeam(match[i].teamA);
		let roomB = collabTeam(match[i].teamB);
		migrate(roomA , roomB);
	}
	updateReadyRoom();
}

let collabTeam=function(Team){
	if(Team.length == 1){
		return Team[0];
	}
	
	roomName = Team[0].player[0].id;
	gameRoom[roomName] = {name:roomName ,player:[] , status:1 , parent:null };
	for(let i=0; i < Team.length ; i++){
		for(let j=0; j < TeamA[i].player.length; j++){
			gameRoom[roomName].player.push(Team[i].player[j]);
			io.of("/game").sockets[TeamA[i].player[j].id].leave(Team[i].name);
			io.of("/game").sockets[TeamA[i].player[j].id].join(roomName);
		}
		delete gameRoom[Team[i].name];
	}

	io.of("/game").in(roomName).emit('team', roomName);
	return gameRoom[roomName];
	
}

let migrate=function(roomA , roomB){
	roomName = roomA.player[0].id+'#GAME';
	gameRoom[roomName] = {name:roomName , status:2 , child:[] };
	
	roomA.parent = roomName ;
	roomA.lastPlayer = 0;
	roomA.status = 2;
	gameRoom[roomName].child.push(roomA);
	roomB.parent = roomName ;
	roomB.lastPlayer = 0;
	roomB.status = 2;
	gameRoom[roomName].child.push(roomB);
	
	for(let i=0; i < roomA.player.length ; i++){
		io.of("/game").sockets[roomA.player[i].id].join(roomName);
		io.of("/game").sockets[roomB.player[i].id].join(roomName);
	}
	gameRoom[roomName].roomMaster = roomA.player[0].user.name;
	gameRoom[roomName].lastTim = 0;
	console.log("Game Play");
	io.of("/game").in(roomName).emit('playing', roomName , gameRoom[roomName]);
}

let getNextPlayer=function(room){
	if(room==null){
		return "";
	}
	room.lastTim = room.lastTim == 1? 0:1;
	let team = room.child[room.lastTim];
	for(let i=0; i < team.player.length; i++){
		team.lastPlayer++;
		team.lastPlayer = team.lastPlayer >= team.player.length ? 0 : team.lastPlayer;
		if(team.player[team.lastPlayer].online){
			break;
		}
	}
	return team.player[team.lastPlayer].user.name;
	
}

let informPlayerInGame=function(roomName){
	io.of("/game").in(roomName).emit('listPlayer', roomName , gameRoom[roomName]);
}

let insert=function(array , value , index){
	
	if(array[index] == null){
		array[index] = {teamA : [] , teamB :[]}
	}
	let a = teamValue(array[index].teamA);
	let b = teamValue(array[index].teamB);
	if(value.player.length + a <= 3 && a <= b){
		array[index].teamA.push(value);
	}else if(value.player.length + b <= 3){
		array[index].teamB.push(value);
	}else{
		insert(array , value, index+1);
	}
	
}
let teamValue=function(team){
	let len = 0;
	for(let i=0; i < team.length; i++){
		len+=team[i].player.length;
	}
	return len;
}
let updateReadyRoom = function(){
	let list = [];
	for(let key in gameRoom){
		if(gameRoom[key].status == 1){
			list.push(gameRoom[key]);
		}
	}
	readyRoom = list;
}


let broadcastRoomList = function(){
	//console.log(gameRoom);
	let list = [];
	for(let key in gameRoom){
		if(gameRoom[key].status == 0){
			list.push(gameRoom[key]);
		}
	}
	
	io.of("/lobby").emit( "roomListUpdate" , list);
	console.log("roomBroadcasted");
}

//delete myArray["lastname"];
io.of("/lobby").on('connection', function(socket){
	console.log('a user connected on lobby namespace :',socket.id);
	let list = [];
	for(let key in gameRoom){
		if(gameRoom[key].status == 0){
			list.push(gameRoom[key]);
		}
	}
	io.of("/lobby").sockets[socket.id].emit( "roomListUpdate" , list);
	io.of("/lobby").sockets[socket.id].emit( "lobbyMessage" , {from:"server" , message:"Selamat datang di Wumpus World, bermain bersama teman dan kalahkan musuh bersama!" , color:'#502ACD'});
	console.log('sendListRoom');
	socket.on('lobbyMessage', function(message){
		socket.broadcast.emit('lobbyMessage',message);
	});
	
	
	socket.on('disconnect', function(){
    console.log('user disconnected :',socket.id);
  });
});



io.of("/game").on('connection', function(socket){
  console.log('a user connected on game namespace :',socket.id);
  
  //Wumpus Game<>
  socket.on('mapLoaded' , ( roomName ) =>{
	  isCanStart = true;
	  inputDone = false;
	  if(gameRoom[roomName] == null){
		  console.log('room '+roomName+' doesnt exist!');
		  return;
	  }
	  
	  for(let i=0 ; i < gameRoom[roomName].child.length; i++){
		for(let j=0 ; j < gameRoom[roomName].child[i].player.length; j++){
			if(gameRoom[roomName].child[i].player[j].id == socket.id){
				gameRoom[roomName].child[i].player[j].map = true;
				inputDone = true;
			}
			
			if(isCanStart){
				if(gameRoom[roomName].child[i].player[j].map == false){
					isCanStart = false;
				}
			}else if(inputDone){
				break;
			}
			
		}
		if(!isCanStart && inputDone){
			break;
		}
	  }
	  
	  if(isCanStart){
		  io.of("/game").in(roomName).emit('allPlayerReady'); 
	  }
	  
  })

  socket.on('gameMap' , ( roomName ,map) =>{
	  io.of("/game").in(roomName).emit('playerPos', map.playerMap);
	  io.of("/game").in(roomName).emit('gameMap', map.map);
  })
  socket.on('nextTurn' , ( roomName ) =>{
	  let playerName = getNextPlayer(gameRoom[roomName]);
	  //console.log(playerName);
	  io.of("/game").in(roomName).emit('turn' , playerName  , gameRoom[roomName].lastTim);
  })
  socket.on('movementUpdate' , ( roomName ,data ) =>{
	  //console.log("player moved");
	  socket.broadcast.to(roomName).emit('movementUpdate' , data );
  })
  socket.on('playerLifeUpdate' , (roomName ,data) =>{
	  socket.broadcast.to(roomName).emit('playerLifeUpdate' , data );
  });
  socket.on('goldPosUpdate' , (roomName ,data) =>{
	  socket.broadcast.to(roomName).emit('goldPosUpdate' , data );
  });
  
  
  //Wumpus Game</>
  
  socket.on('joinRoom', function(roomName,user) {
		
		
		let isNew = gameRoom[roomName] == null;
		if(isNew){
			
			gameRoom[roomName] = {name:roomName ,player:[] , status:0 , parent:null , readyPlayer:0};
		}else if(gameRoom[roomName].status != 0){
			socket.emit('rejected', 'Cant join to team, team is already playing');
			return;
		}else if(gameRoom[roomName].player.length >= 3){
			socket.emit('rejected', 'Team have been reach maximal player count');
			return;
		}else{
			for(let i= 0 ; i < gameRoom[roomName].player.length; i++){
				if(gameRoom[roomName].player[i].user.name == user.name){
					socket.emit('playerListUpdate', gameRoom[roomName].player);
					return;
				}
			}
		}
		console.log('user :',socket.id,'joined room',roomName);
		socket.join(roomName);
		gameRoom[roomName].player.push({user:user, id:socket.id, online:true, map:false , ready:false});
		io.of("/game").in(roomName).emit('playerListUpdate', gameRoom[roomName].player);
		
		broadcastRoomList();
		
  });
  socket.on('requestPlayerList', function(roomName) {
	  if(gameRoom[roomName]!= null){
		socket.emit('playerListUpdate', gameRoom[roomName].player);
	  }else{
		socket.emit('failed', 'Tim tidak ditemukan');
	  }
	
  });
  
  socket.on('re-joinRoom', function(roomName,gameRoomName,user) {
		console.log('user :',socket.id,'reconnected to room :',roomName);
		let index = getPlayerIndex(roomName , user.name);
		
		if(index == -1){
			console.log('cant find tim');
			socket.emit('failed', 'Re connect failed you re not enrolled to the room');
			return;
		}
		
		if(gameRoom[roomName].parent != gameRoomName){
			console.log('cant find room',gameRoomName,'should',gameRoom[roomName].parent);
			socket.emit('failed', 'Re connect failed you re not enrolled to the room');
			return;
		}
		
		
		socket.join(roomName);
		socket.join(gameRoomName);
		
		gameRoom[roomName].player[index].id = socket.id
		gameRoom[roomName].player[index].online = true;
		console.log('request for map data');
		io.of("/game").sockets[ socket.id ].emit('playing', gameRoom[roomName].parent , gameRoom[gameRoom[roomName].parent]);
		io.of("/game").sockets[ gameRoom[roomName].player[(index==0?1:0)].id ].emit('requestMapData' , socket.id );
		//io.of("/game").in(roomName).emit('playerListUpdate', gameRoom[roomName].player);
		
  });
  
  socket.on('responseMapData', function(socketID,data) {
		console.log('send for map data');
		io.of("/game").sockets[socketID].emit('playerPos', data.playerMap);
		io.of("/game").sockets[socketID].emit('gameMap', data.map);
		//io.of("/game").sockets[socketID].emit('allPlayerReady'); 
  });
  
  socket.on('play', function(roomName ) {
		//console.log("here")
		if(socket.id != gameRoom[roomName].player[0].id){
			socket.emit('failed', 'Auth you re not master');
			return;
		}
		//console.log("here1")
		isAllPlayerReady = true;
		for(let i=1; i < gameRoom[roomName].player.length; i++){
			isAllPlayerReady = isAllPlayerReady && gameRoom[roomName].player[i].ready;
			
		}
		//console.log("herex")
		
		if(isAllPlayerReady){
			//console.log("herey")
			gameRoom[roomName].status = 1;
			gameRoom[roomName].readyPlayer = gameRoom[roomName].player.length;
			
			for(let i=0; i < gameRoom[roomName].player.length; i++){
				io.of("/game").sockets[gameRoom[roomName].player[i].id].emit('gameScene');
				console.log('emit to game scene ' , gameRoom[roomName].player[i].id);
			}
			//.in(roomName).emit('gameScene');
			//console.log("herez")
			//start match making
			console.log(roomName," Ready with ",gameRoom[roomName].player.length," member");
			broadcastRoomList();
		}else{
			console.log("here",isAllPlayerReady )
			socket.emit('failed', 'Waiting for other player');
		}
		
  });
  
  // socket.on('re-connect', function(username) {
		
		// let loc = getPlayerRoomLocationFromUsername(username);
		// socket.emit('re-connect' , loc );
		// if(loc == null){
			// socket.emit('re-connect' ,  null );
		// }else{
			// socket.emit('re-connect' , {tim: gameRoom[loc.key].name , room:gameRoom[loc.key].parent.name} );
		// }
		
		// socket.to(roomName).emit('message', data);
  // });
  
  socket.on('readyToPlay', function(roomName) {
		for(let i=0; i < gameRoom[roomName].player.length; i++){
			if(i==0){	continue;	}
			if(gameRoom[roomName].player[i].id == socket.id){
				gameRoom[roomName].player[i].ready = !gameRoom[roomName].player[i].ready;
				break;
			}
		}
		
		io.of("/game").in(roomName).emit('playerListUpdate', gameRoom[roomName].player);
		
  });
  socket.on('kickPlayer' , function(roomName , userName){
		if(socket.id != gameRoom[roomName].player[0].id){
			socket.emit('failed', 'Auth you re not master');
			return;
		}
		for(let i=1; i < gameRoom[roomName].player.length; i++){
			if(gameRoom[roomName].player[i].user.name == userName){
				io.of("/game").sockets[ gameRoom[roomName].player[i].id ].emit('kicked');
				break;
			}
		}
  });
  
  
  socket.on('onGameScene', function(roomName) {
		console.log(socket.id,"On Game  Scene ");
		if(gameRoom[roomName] == null || gameRoom[roomName].status!=1){
			//room is already played or not in game
			return;
		}
		gameRoom[roomName].readyPlayer--;
		if(gameRoom[roomName].readyPlayer ==0){
			readyRoom.push(gameRoom[roomName]);
			MatchMaking();
		}
  });
  socket.on('prepareMessage', function(message,roomName){
		socket.broadcast.to(roomName).emit('prepareMessage',message);
  });
  
  socket.on('message', function(roomName , data) {
		socket.to(roomName).emit('message', data);
  });
  
  socket.on('cancelplay', function(roomName) {
		if(gameRoom[roomName] == null){
			
			return;
		}
		console.log(roomName," canceled with ",gameRoom[roomName].player.length," member, status:",gameRoom[roomName].status);
		if(gameRoom[roomName].status != 1){
			socket.emit('failed', 'Tidak bisa menggagalkan Match making/Play');
			return;
		}
		for(let i=0; i < gameRoom[roomName].player.length; i++){
			gameRoom[roomName].player[i].ready = false;
		}
		gameRoom[roomName].status = 0;
		io.of("/game").in(roomName).emit('backToPrepare');
		updateReadyRoom();
		broadcastRoomList();
		//io.of("/game").in(roomName).emit('playerListUpdate', gameRoom[roomName].player);
  });

  
  socket.on('disconnect', function(){
    
	let location = getPlayerRoomLocation(socket.id);
	if(location != null){
		
		if(gameRoom[location.key].status == 0 || gameRoom[location.key].status == 1){
			gameRoom[location.key].player.splice(location.index, 1); 
			if(gameRoom[location.key].status == 1){
				io.of("/game").in(location.key).emit('backToPrepare');
			}
			gameRoom[location.key].status = 0;
			
			
		}else if(gameRoom[location.key].status == 2){
			gameRoom[location.key].player[location.index].online = false;
			if(isAllPlayerOffline(gameRoom[location.key])){
				gameRoom[location.key].player = [];
				let parentRoom = gameRoom[gameRoom[location.key].parent];
				if(parentRoom != null){
					console.log("force win");
					if(parentRoom.child[0]==gameRoom[location.key]){
						io.of("/game").in(parentRoom.child[1].name).emit('forceWin', "All enemy disconnected");
						for(let i=0; i < parentRoom.child[1].player.length; i++){
							if(parentRoom.child[1].player[i].online){
								io.of("/game").sockets[parentRoom.child[1].player[i].id].leave(parentRoom.name);
							}
						}
					}else{
						io.of("/game").in(parentRoom.child[0].name).emit('forceWin',  "All enemy disconnected");
						for(let i=0; i < parentRoom.child[0].player.length; i++){
							if(parentRoom.child[0].player[i].online){
								io.of("/game").sockets[parentRoom.child[0].player[i].id].leave(parentRoom.name);
							}
						}
					}
					console.log('room deleted :',parentRoom.name);
					delete gameRoom[parentRoom.name];
					
					//console.log(gameRoom);
				}
			}
			
		}
		
		if(gameRoom[location.key].player.length == 0 ){
			let parentRoom = gameRoom[gameRoom[location.key].parent];
			
			delete gameRoom[location.key];
			if(parentRoom != null){
				if(parentRoom.child[0]==null&&parentRoom.child[1]==null){
					console.log('room deleted :',parentRoom.name);
					delete gameRoom[parentRoom.name];
				}
			}
			updateReadyRoom();
			broadcastRoomList();
			console.log('room deleted :',location.key);
		}else{
			console.log('user disconnected from room :',location.key);
			io.of("/game").in(location.key).emit('playerListUpdate', gameRoom[location.key].player);
		}
		
		
	}
  });
});

isAllPlayerOffline=function(room){
	for(let i=0; i < room.player.length; i++){
		if(room.player[i].online){
			return false;
		}
	}
	return true;
}

getPlayerRoomLocation = function(socketID){
	for(var key in gameRoom){
		if(gameRoom[key].player==undefined||gameRoom[key].player==null){continue;}
		for(let i =0; i< gameRoom[key].player.length ; i++){
			if(gameRoom[key].player != null){
				if(gameRoom[key].player[i].id == socketID){
					return {key : key , index : i}
				}
			}
		}
	}
	return null;
}

getPlayerRoomLocationFromUsername = function(username){
	for(var key in gameRoom){
		for(let i =0; i< gameRoom[key].player.length ; i++){
			if(gameRoom[key].player != null){
				if(gameRoom[key].player[i].user.name == username){
					return {key : key , index : i}
				}
			}
		}
	}
	return null;
}

getPlayerIndex=function(roomName , userName){
	for(let i =0; i< gameRoom[roomName].player.length ; i++){
		if(gameRoom[roomName].player[i].user.name == userName){
			return i;
		}
	}
	return -1;
}



http.listen(port, function(){
  console.log('listening on port :',port);
});
